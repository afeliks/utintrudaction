﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using TestApp.Level5;

namespace TestAppUT.Level5
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        public async Task Test_Sum()
        {
            var temp = new Temp();

            var result = await temp.Sum(1, 2);

            Assert.AreEqual(3, result);
        }
    }
}
