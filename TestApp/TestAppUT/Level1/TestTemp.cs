﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestApp.Level1;

namespace TestAppUT.Level1
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        public void Test_Sum()
        {
            var temp = new Temp();
            const double temp1 = 2;
            const double temp2 = 3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(5, result, "Wrong sum");
        }

        [TestMethod]
        public void Test_Sum_OneNegativeTemp()
        {
            var temp = new Temp();
            const double temp1 = 2;
            const double temp2 = -3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(-1, result, "Wrong sum");
        }

        [TestMethod]
        public void Test_Sum_BothNegativeTemp()
        {
            var temp = new Temp();
            const double temp1 = -2;
            const double temp2 = -3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(-5, result, "Wrong sum");
        }
    }
}
