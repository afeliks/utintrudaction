﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Reflection;
using TestApp.Level6;

namespace TestAppUT.Level6
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        public void Test_Sum()
        {
            var type = typeof(Temp);
            var temp = new Temp();

            MethodInfo setReturnF = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Where(x => x.Name == "SetReturnF" && x.IsPrivate).First();

            setReturnF.Invoke(temp, new object[] { true });

            var result = temp.Sum(1, 1);

            Assert.AreEqual(35.6, result);
        }

        [TestMethod]
        public void Test_Sum_WithoutSetReturnF()
        {
            var type = typeof(Temp);
            var temp = new Temp();

            FieldInfo returnF = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                    .Where(x => x.Name == "_returnF").First();

            returnF.SetValue(temp, true);

            var result = temp.Sum(1, 1);

            Assert.AreEqual(35.6, result);
        }
    }
}
