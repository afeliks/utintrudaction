﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TestApp.Level7;
using Pose;

namespace TestAppUT.Level7
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        public void TestSum()
        {
            Shim tempShim = Shim.Replace(() => Is.A<Temp>().IsFRegion())
                .With(delegate (Temp @this) { return true; });

            PoseContext.Isolate(() =>
            {
                Temp temp = new Temp();
                var result = temp.Sum(1, 1);

                Assert.AreEqual(35.6, result);
            },tempShim);
        }
    }
}
