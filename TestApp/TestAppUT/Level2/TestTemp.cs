﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TestApp.Level2;

namespace TestAppUT.Level2
{
    [TestClass]
    public class TestTemp
    {
        Temp temp;

        [TestInitialize]
        public void TestInitializer()
        {
            temp = new Temp();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            temp = null;
        }

        #region Level1 methods
        [TestMethod]
        public void Test_Sum()
        {
            const double temp1 = 2;
            const double temp2 = 3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(5, result, "Wrong sum");
        }

        [TestMethod]
        public void Test_Sum_OneNegativeTemp()
        {
            const double temp1 = 2;
            const double temp2 = -3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(-1, result, "Wrong sum");
        }

        [TestMethod]
        public void Test_Sum_BothNegativeTemp()
        {
            const double temp1 = -2;
            const double temp2 = -3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(-5, result, "Wrong sum");
        }
        #endregion Level1 methods

        [TestMethod]
        public void Test_Sum_InC()
        {
            const double temp1 = 2;
            const double temp2 = 3;

            var result = temp.Sum(temp1, temp2);

            Assert.AreEqual(5, result, "Wrong sum");
        }

        [TestMethod]
        public void Test_Sum_InF()
        {
            const double temp1 = 2;
            const double temp2 = 3;

            var result = temp.Sum(temp1, temp2, true);

            Assert.AreEqual(41, result, "Wrong sum in F");
        }
    }
}
