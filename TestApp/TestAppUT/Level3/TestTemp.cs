﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TestApp.Level3;

namespace TestAppUT.Level3
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Test_Sum_FirstNegativeTempThrowException()
        {
            var temp = new Temp();
            const double temp1 = -2;
            const double temp2 = 3;

            temp.Sum(temp1, temp2);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Test_Sum_SecondNegativeTempThrowException()
        {
            var temp = new Temp();
            const double temp1 = 2;
            const double temp2 = -3;

            temp.Sum(temp1, temp2);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void Test_Sum_BothNegativeTempThrowException()
        {
            var temp = new Temp();
            const double temp1 = -2;
            const double temp2 = -3;

            temp.Sum(temp1, temp2);
        }
    }
}
