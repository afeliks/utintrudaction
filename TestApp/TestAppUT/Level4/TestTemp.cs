﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TestApp.Level4;

namespace TestAppUT.Level4
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        public void TestSum()
        {
            const double returnValue = 12;

            var converterMock = new Mock<ITempConverter>();
            converterMock.Setup(f => f.FromCIntoF(It.IsAny<double>())).Returns(returnValue);

            ITempConverter converter = converterMock.Object;

            // ITempConverter converter = Mock.Of<ITempConverter>(c => c.FromCIntoF(It.IsAny<double>()) == returnValue);

            var temp = new Temp(converter);

            var result = temp.Sum(1, 1, true);
            Assert.AreEqual(returnValue, result);
        }

        [TestMethod]
        public void TestSum_VerifyIfConvertIsCalled()
        {
            var converterMock = new Mock<ITempConverter>();
            converterMock.Setup(f => f.FromCIntoF(It.IsAny<double>())).Returns(It.IsAny<double>());

            ITempConverter converter = converterMock.Object;

            var temp = new Temp(converter);

            temp.Sum(1, 1, true);

            converterMock.Verify(f => f.FromCIntoF(It.IsAny<double>()), Times.Once);
        }

        [TestMethod]
        public void TestSum_VerifyIfConvertIsNotCalled()
        {
            var converterMock = new Mock<ITempConverter>();

            converterMock.Setup(f => f.FromCIntoF(It.IsAny<double>())).Returns(It.IsAny<double>());

            ITempConverter converter = converterMock.Object;

            var temp = new Temp(converter);

            temp.Sum(1, 1);

            converterMock.Verify(f => f.FromCIntoF(It.IsAny<double>()), Times.Never);
        }
    }
}
