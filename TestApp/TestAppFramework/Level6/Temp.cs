﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAppFramework.Level6
{
    public class Temp
    {
        private bool _returnF = false;

        public double Sum(double temp1, double temp2)
        {
            if (temp1 < 0 || temp2 < 0)
            {
                throw new FormatException("Temperature can not be negative");
            }

            var sum = temp1 + temp2;

            if (_returnF)
            {
                return (1.8 * sum) + 32;
            }

            return sum;
        }

        private void SetReturnF(bool value)
        {
            _returnF = value;
        }
    }
}
