﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestAppFramework.Level6;

namespace TestAppUTFramework.Level6
{
    [TestClass]
    public class TestTemp
    {
        [TestMethod]
        public void Test_Sum()
        {
            var temp = new Temp();
            var privateObject = new PrivateObject(temp);

            privateObject.Invoke("SetReturnF", true);

            var result = temp.Sum(1, 1);

            Assert.AreEqual(35.6, result);
        }

        [TestMethod]
        public void Test_Sum_WithoutSetReturnF()
        {
            var temp = new Temp();
            var privateObject = new PrivateObject(temp);

            privateObject.SetField("_returnF", true);

            var result = temp.Sum(1, 1);

            Assert.AreEqual(35.6, result);
        }
    }
}
