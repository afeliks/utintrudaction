﻿using System.Threading.Tasks;

namespace TestApp.Level5
{
    public class Temp
    {
        public async Task<double> Sum(double temp1, double temp2)
        {
            await Task.Delay(1000);

            return temp1 + temp2;
        }
    }
}
