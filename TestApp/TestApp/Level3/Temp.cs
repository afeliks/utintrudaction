﻿using System;

namespace TestApp.Level3
{
    public class Temp
    {
        public double Sum(double temp1, double temp2, bool returnF = false)
        {
            if(temp1 < 0 || temp2 < 0)
            {
                throw new FormatException("Temperature can not be negative");
            }

            var sum = temp1 + temp2;

            if (returnF)
            {
                return (1.8 * sum) + 32;
            }

            return sum;
        }
    }
}
