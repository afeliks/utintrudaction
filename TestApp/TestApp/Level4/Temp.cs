﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApp.Level4
{
    public class Temp
    {
        ITempConverter TempConverter { get; }

        public Temp(ITempConverter tempConverter)
        {
            TempConverter = tempConverter;
        }

        public double Sum(double temp1, double temp2, bool returnF = false)
        {
            if (temp1 < 0 || temp2 < 0)
            {
                throw new FormatException("Temperature can not be negative");
            }

            var sum = temp1 + temp2;

            if (returnF)
            {
                return TempConverter.FromCIntoF(sum);
            }

            return sum;
        }
    }
}
