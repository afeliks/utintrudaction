﻿namespace TestApp.Level4
{
    public interface ITempConverter
    {
        double FromCIntoF(double temp);
    }
}
