﻿namespace TestApp.Level2
{
    public class Temp
    {
        public double Sum(double temp1, double temp2, bool returnF = false)
        {
            var sum = temp1 + temp2;

            if (returnF)
            {
                return (1.8 * sum) + 32;
            }

            return sum;
        }
    }
}
